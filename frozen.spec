%global debug_package %{nil}
 
Name:           frozen
Version:        1.1.1
Release:        2
Summary:        A header-only, constexpr alternative to gperf for C++14 users
  
License:        Apache-2.0
URL:            https://github.com/serge-sans-paille/frozen
Source0:        %{url}/archive/%{version}/%{name}-%{version}.tar.gz
# Fixes FTBFS, already present in upstream main branch.
Patch0:         includes.patch
Patch1:         079f73cc5c6413127d47f325cbb34a607e2cb030.patch
# related: https://github.com/serge-sans-paille/frozen/pull/167
Patch2:         frozen-fix-arch-in-cmake.patch
   
BuildRequires: gcc-c++
BuildRequires: cmake
    
%description
Header-only library that provides 0 cost initialization
for immutable containers, fixed-size containers, and
various algorithms.
     
%package devel
Summary:        Development files for %{name}
Requires:       pkgconfig
Provides:       %{name}-static = %{version}-%{release}
      
%description devel
Development files for %{name}.
       
%prep
%setup -q
%patch 0 -p0
%patch 1 -p1
%patch 2 -p1

	  
%build
%cmake -DCMAKE_BUILD_TYPE=Release
%cmake_build
%check

%install
%cmake_install
	   
%files devel
%license LICENSE
%doc examples/ AUTHORS README.rst
%{_includedir}/frozen/
%{_datadir}/cmake/%{name}/
	    
%changelog
* Tue Feb 18 2025 peijiankang <peijiankang@kylinos.cn> - 1.1.1-2
- fix build error about build

* Mon Apr 8 2024 wyxaihjr <yuexiang.oerv@isrc.iscas.ac.cn> - 1.1.1-1
- init packages
